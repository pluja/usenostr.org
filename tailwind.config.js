
/** @type {import("tailwindcss").Config} */

module.exports = {
  content: [
    './*.html',
    './relay.html',
    './accounts.html',
  ],
  theme: {
      extend: {
        fontFamily: {
          'lato': ['Lato'],
          'unb': ['Unbounded'],
          'chivo': ['Chivo'],
        }
      },
      fontFamily: {
        'sans': ['Lato', 'system-ui'],
        'serif': ['ui-serif', 'Georgia'],
        'mono': ['Chivo', 'SFMono-Regular'],
        'display': ['Lato'],
        'body': ['"Lato"'],
      }
  }
}
      
  